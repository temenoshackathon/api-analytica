# API-Analytica

Temenos API Analytica

API Analytica is a web application, submitted for Temenos Hackathon Event on October ‘19, which provide the information of every request comes from End User to Temenos Products (T24), by monitoring the IRIS (Interaction Framework) layer and store request details into the MySql database. Restful http-get API’s helps to interact with Analytical data in a UI, which represents the data as various graphs by use of High-charts.